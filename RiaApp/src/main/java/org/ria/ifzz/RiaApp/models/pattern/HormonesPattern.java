package org.ria.ifzz.RiaApp.models.pattern;

/**
 * represents hormones patterns which should be founded in uploaded file
 */
public class HormonesPattern {

    public static final double[] CORTISOL_PATTERN = {
            1.250,
            1.250,
            2.500,
            2.500,
            5.000,
            5.000,
            10.000,
            10.000,
            20.000,
            20.000,
            40.000,
            40.000,
            80.000,
            80.000
    };
}
